Workflow for Bibliophile
========================

Components:
==========

1. main.js is mentioned as the entry point to the app and this contains the "handler" that AWS lambda uses when it starts executing.
2. handlers directory contains entry point to different lambda handlers.
3. helpers will contain helper functions. Right now there are 3 files: one for handling get/set/remove things via goodreads API, second one for fetching from isbndb, third one for oauth.
4. whenever you require a particular module/library to be present, install that with npm install <library> --save. So that it gets updated inside package.json.
5. Add test cases to test and use lambda-local.