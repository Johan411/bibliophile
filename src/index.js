// Basic formatting
// ================
// 1. File specific variables here at the top.
// 2. Use all caps for fixed strings.
// 3. Lists are better if they are in multiple lines.
// 4. Use underscore for multi-word variable names.

var Alexa = require("alexa-sdk");
var Goodreads = require("../helpers/goodreads_api");
var Ddb = require("../helpers/dynamodb_crud");
var SKILL_NAME = "bibliophile";
var APP_ID = "amzn1.ask.skill.556a478d-302e-4563-a94a-31fa34be78f4";

// Basic setup for Alexa. This handler needs to be exported, so that AWS lambda can use it.
// Main entrypoint for AWS lambda.

exports.handler = function(event, context, callback) {
    console.log("Inside the handler");
    console.log("The event request type is " + event.request.type);
    console.log("The event is " + event.request);
    var alexa = Alexa.handler(event, context);
    console.log("Something over here: " + event.request.type);

    alexa.appId = APP_ID;

    // Multiple handlers can be registered

    alexa.registerHandlers(handlers);
    alexa.execute();
}

var handlers = {
    "LaunchRequest": function() {
        console.log("Inside LaunchRequest");
        this.emit("BibliophileHelp");
    },
    "SessionEndedRequest": function() {
        console.log("Thanks for using bibliophile!");
	this.emit(":tell", "I'll see you later!");
    },
    "BibliophileHelp": function() {
        // Don't end the session. Wait for user to respond back with something.
        this.emit(":ask", "Hey there! Welcome to Bibliophile, we help you to get connected with one of the most popular social cataloging websites, Goodreads.", "You can start off by asking about details of an author or book");
    },
    "GetBookDetailsIntent": function() {
        if (!this.event.request.intent.slots.bookName.hasOwnProperty("value")) {
            console.log("No book to search for. Exiting ...");
            this.emit(":tell", "You haven't specified a book name. Please try again.");
        }
        else {
            this.emit("GetBookDetails")
        }
    },
    "GetBookDetails": function() {
        console.log("Inside GetBookDetails");
        var bookName = this.event.request.intent.slots.bookName.value;
        Ddb.ddb_read_entry("Bookdetails_table", {
            bookname: bookName
        }, this, check_in_cache);

        function check_in_cache(context, data) {
            if (data == null) {
                console.log("Not present in db");
                Goodreads.goodreads_getBookDetails(bookName, context);
            } else {
                Goodreads.sendDataToAlexa(context, ":tell", data.Item.details);
                console.log(data);
            }
        };
    },
    "GetAuthorDetailsIntent": function() {
        if (!this.event.request.intent.slots.authorName.hasOwnProperty("value")) {
            console.log("No author name specified. Exiting ...");
            this.emit(":tell", "You haven't specified an author name. Please try again.");
        }
        this.emit("GetAuthorDetails");
    },
    "GetAuthorDetails": function() {
        console.log("Inside GetAuthorDetails");
        var authorName = this.event.request.intent.slots.authorName.value;
        console.log(authorName);
        Ddb.ddb_read_entry("Authordetails_table", {
            authorname: authorName
        }, this, check_in_cache);

        function check_in_cache(context, data) {
            if (data == null) {
                console.log("Not present in db");
                Goodreads.goodreads_getAuthorDetails(authorName, context);
            } else {
                Goodreads.sendDataToAlexa(context, ":tell", data.Item.details);
                console.log(data);
            }
        };
    },
    "FollowAuthorIntent": function() {
        if (this.event.session.user.accessToken == undefined) {
            this.emit(":tellWithLinkAccountCard", "Please allow Bibliophile to access your Goodreads account for this functionality.")
        } 
        else if (!this.event.request.intent.slots.authorName.hasOwnProperty("value")) {
            console.log("No author to follow. Exiting ...");
            this.emit(":tell", "You haven't specified an author name to follow. Please try again.");
        }
        else {
            this.emit("FollowAuthor");
        }
    },
    "FollowAuthor": function() {
        console.log("Inside FollowAuthor");
        var authorName = this.event.request.intent.slots.authorName.value;
        console.log(authorName);
        Goodreads.goodreads_followAuthor(authorName, this);
        console.log("Returned from FollowAuthor");
    },
    "BooksOwnedIntent": function() {
        if (this.event.session.user.accessToken == undefined) {
            this.emit(":tellWithLinkAccountCard", "Please allow Bibliophile to access your Goodreads account for this functionality.")
        } else {
            this.emit("BooksOwned");
        }
    },
    "BooksOwned": function() {
        console.log("Inside goodreads_booksOwned");
        Goodreads.goodreads_booksOwned(this);
        console.log("Returned from goodreads_booksOwned");
    },
    "Unhandled": function() {
        // this is when nothing matches the prescribed utterances
        console.log("Inside defaultintents");
        this.emit(":tell", "Sorry, I didn't quite understand that.");
    },
    "AMAZON.HelpIntent": function() {
        var speech_output = "Bibliophile here! You can start by asking me about books and authors that I know of. I can also help you get all the books you have added to your owned books list by asking something as simple as Alexa, ask bibliophile daily to get my books and much more. Feel free to stop me by asking bibliophile daily to stop. You can say Alexa exit to stop the skill. So how about giving me a try?";
        var speech_reprompt = "Bibliophile here! You can start by asking me about books and authors that I know of. How can I help you today?";
        this.emit(":ask", speech_output, speech_reprompt);
    },
    "AMAZON.StopIntent": function() {
        this.emit(":tell", "Alrighty, I'll stop.");
    },
    "AMAZON.CancelIntent": function() {
        this.emit(":tell", "Maybe we can do that some other time.");
    }
}


// var default_handlers = require("./handlers/default_handlers");
