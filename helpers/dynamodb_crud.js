var AWS = require("../src/node_modules/aws-sdk");

// use this only for testing & ensure that dynamodb is running locally on port 8000
//AWS.config.update({
//    region: "us-west-2",
//    endpoint: "http://localhost:8000"
//});

// dbname to ARN mapping (in case it's required)
const db_to_ARN = {
    "Authordetails_table": "arn:aws:dynamodb:us-east-1:111815004222:table/Authordetails_table   ",
    "Bookdetails_table": "arn:aws:dynamodb:us-east-1:111815004222:table/Bookdetails_table",
    "Oauth_table": "arn:aws:dynamodb:us-east-1:111815004222:table/Oauth_table"
};

var dynamodb = new AWS.DynamoDB();

var params = {
    TableName: "Authordetails_table",
    KeySchema: [{
            AttributeName: "authorname",
            KeyType: "HASH"
        }],
    AttributeDefinitions: [{
        AttributeName: "authorname",
        AttributeType: "S"
    }],
    ProvisionedThroughput: {
        ReadCapacityUnits: 5,
        WriteCapacityUnits: 5
    }
};

var param_gen = function(tablename, operation_type, item) {
    if (operation_type == "add") {
        var params = {
            TableName: tablename,
            Item: item
        };
    }
    else {
        var params = {
            TableName: tablename,
            Key: item
        };
    }
    return params;
};

var ddb_create_table = function(params) {
    dynamodb.createTable(params, function(err, data) {
        if (err) {
            console.error("Unable to create table. Error JSON:", JSON.stringify(err, null, 2));
        } else {
            console.log("Created table. Table description JSON:", JSON.stringify(data, null, 2));
        }
    });
};

var ddb_add_entry = function(tablename, item, context, callback) {
    var params = param_gen(tablename, "add", item);
    console.log(params);
    var docClient = new AWS.DynamoDB.DocumentClient();
    docClient.put(params, function(err, data) {
        if (err) {
            console.error("Unable to add item. Error JSON:", JSON.stringify(err, null, 2));
            callback(context, null);
            return;
        } else {
            console.log("Added item:", JSON.stringify(data, null, 2));
            callback(context, data);
            return;
        }
    });
};

// Returns null to the callback function if data isn't present. Refer to tester.js for examples
var ddb_read_entry = function(tablename, item, context, callback) {
    var params = param_gen(tablename, "read", item);
    var docClient = new AWS.DynamoDB.DocumentClient();
    docClient.get(params, function(err, data) {
        if (err) {
            console.error("Unable to read item. Error JSON:", JSON.stringify(err, null, 2));
        } else {
            // console.log(data.Item);
            if (data.Item == undefined){
                callback(context, null);
                return;
            }
            console.log("GetItem succeeded:", JSON.stringify(data, null, 2));
            callback(context, data);
            return; 
        }
    });
};

var ddb_delete_entry = function(tablename, item, context, callback) {
    var params = param_gen(tablename, "delete", item);
    var docClient = new AWS.DynamoDB.DocumentClient();
    docClient.delete(params, function(err, data) {
        if (err) {
            console.error("Unable to delete item. Error JSON:", JSON.stringify(err, null, 2));
        } else {
            console.log("DeleteItem succeeded:", JSON.stringify(data, null, 2));
        }
        console.log("Done with deletion");
        callback(context, data);
    });
};

module.exports = {
    ddb_create_table: ddb_create_table,
    ddb_add_entry: ddb_add_entry,
    ddb_read_entry: ddb_read_entry,
    ddb_delete_entry: ddb_delete_entry
};
