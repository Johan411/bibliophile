var handlers = {
	"AMAZON.HelpIntent": function(){
		var speech_output = "Hey there! You can say blah blah blah. What can I help you with today?";
		var speech_reprompt = "What can I help you with today?";
		this.emit(":ask", speech_output, speech_reprompt);
	}
	"AMAZON.StopIntent": function(){
		this.emit(":tell", "Goodbye!");
	}
	"AMAZON.CancelIntent": function(){
		this.emit(":tell", "Goodbye!");
	}
}