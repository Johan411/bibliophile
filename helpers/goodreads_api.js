var http_request = require("../src/node_modules/request");
var parseString = require('../src/node_modules/xml2js').parseString;
var Ddb = require("./dynamodb_crud");


var KEY = "key=pH3bEyXe9y7Rc47bRfLg";
const HOST_NAME = "https://www.goodreads.com/";

// Fetch details of author

function sendDataToAlexa(context, type, outputSpeech) {
    context.emit(type, outputSpeech);
}

var goodreads_getAuthorDetails = function(authorName, object) {

    var authorID = "";
    var outputSpeech = "";

    var formattedAuthorName = authorName.replace(/ /g, "%20");

    var getAuthorID = new Promise(function(resolve, reject) {
        var options = {
            "method": "GET",
            "url": HOST_NAME + "/api/author_url/+" + formattedAuthorName + "?" + KEY
        };
        http_request(options, function(error, response, body) {
            parseString(body.toString(), function(err, result) {
                if (result.GoodreadsResponse.author != undefined) {
                    authorID = result.GoodreadsResponse.author[0].$.id;
                }

            })
            if (authorID != "") {
                resolve(authorID);
            } else {
                reject(error);
            }

        });
    });

    callback = function() {
        var gender = "";
        var fans_count = "";
        var home_town = "";
        var book = "";
        var published_year = "";
        var options = {
            "method": "GET",
            "url": HOST_NAME + "/author/show/" + authorID + "?format=xml&" + KEY
        };

        http_request(options, function(error, response, body) {
            parseString(body.toString(), function(err, result) {
                if (err) {
                    console.log("Author not found");
                    return;
                } else {
                    console.log(result.GoodreadsResponse);
                    var authorDetails = result.GoodreadsResponse.author[0];
                    authorName = authorDetails.name[0].toString();
                    gender = authorDetails.gender.toString();
                    fans_count = authorDetails.fans_count[0]._.toString();
                    home_town = authorDetails.hometown[0].toString();
                    book = authorDetails.books[0].book[0];
                    console.log(book);
                    var re = [
                        /(<\s*(\S*)\s*\>)?/g,
                        /(<\s*(\S*)\s*\/>)?/g,
                        /(<\s*\/(\S*)\s*\>)?/g,
                        /[^\.|\,|\'|A-Z|a-z|0-9|\s*]/g,
                    ];

                    book.description[0] = book.description[0].replace(/ (\&)+ /g, " and ");
                    authorName = authorName.replace(/ (\&)+ /g, " and ");
                    book.title[0] = book.title[0].replace(/ (\&)+ /g, " and ");
                    book.publisher[0] = book.publisher[0].replace(/ (\&)+ /g, " and ");

                    for (i = 0; i < re.length; i++) {
                        book.description[0] = book.description[0].replace(re[i], "");
                        authorName = authorName.replace(re[i], "");
                        book.title[0] = book.title[0].replace(re[i], "");
                        book.publisher[0] = book.publisher[0].replace(re[i], "");
                    }
                }
            })
            
            outputSpeech = authorName + " is a " + gender + ". I found that the the author has " +
                fans_count + " followers. Hometown is " + home_town + " .One of the best works of the author is " +
                book.title[0] + " re published in the year " + book.published[0] + " by the publishing company " +
                book.publisher[0] + ". I did find a synopsis of the same and here it goes. " + book.description[0];

            Ddb.ddb_add_entry("Authordetails_table", {
                "authorname": authorName,
                "details": outputSpeech
            }, object, function(res, err) {
                if (res) {
                    console.log("Added to db");
                } else {
                    console.log("Unable to add");
                }
                if(outputSpeech!="")
                {
                    sendDataToAlexa(object, ":tell", outputSpeech);
                }
                else
                {
                    sendDataToAlexa(object, ":tell", "No information of the author found");
                }
                
            });
            return;
        });
    }
    getAuthorID.then(callback).catch((err) => {
        //console.log("Error is" + err);
        object.emit(":tell", "Author not found");
    });
    return;
}

var goodreads_getBookDetails = function(bookName, object) {

    console.log("Hello from getBookDetails");

    var formattedBookName = bookName.replace(/ /g, "%20");
    var bookData = "";
    var ratings_count = "";
    var book_id = "";
    var average_rating = "";
    var review_count = "";
    var year = "";
    var author_name = "";


    var getBookData = new Promise(function(resolve, reject) {
        var options = {
            "method": "GET",
            "url": HOST_NAME + "/search/index.xml?" + KEY + "&q=" + formattedBookName
        };
        http_request(options, function(error, response, body) {
            if (body != undefined) {
                parseString(body, function(err, result) {

                    searchResult = result.GoodreadsResponse.search[0];
                    console.log(searchResult['total-results']);
                    if (searchResult['total-results'] != 0) {

                        if (result.GoodreadsResponse.search[0].results[0] != undefined) {
                            console.log("Hello");

                            bookData = result.GoodreadsResponse.search[0].results[0].work[0];
                            ratings_count = bookData.ratings_count[0]._;
                            book_id = bookData.best_book[0].id[0]._;
                            console.log(book_id);
                            average_rating = bookData.average_rating[0];
                            review_count = bookData.text_reviews_count[0]._;
                            year = bookData.original_publication_year[0]._;
                            author_name = bookData.best_book[0].author[0].name[0];
                        }
                    }

                })
            }
            if (bookData != "") {

                resolve(book_id);
            } else {
                reject(error);
            }

        });

    });

    var callbackBook = function(book_id) {
        var description = "";
        var options = {
            "method": "GET",
            "url": HOST_NAME + "book/show.xml?id=" + book_id + "&" + KEY
        };
        http_request(options, function(error, response, body) {
            parseString(body.toString(), function(err, result) {
                if (err) {
                    console.log("Author not found");
                    return;
                } else {

                    var re = [
                        /(<\s*(\S*)\s*\>)?/g,
                        /(<\s*(\S*)\s*\/>)?/g,
                        /(<\s*\/(\S*)\s*\>)?/g,
                        /[^\.|\,|A-Z|a-z|0-9|\s*]/g
                    ];

                    for (i = 0; i < re.length; i++) {

                        result.GoodreadsResponse.book[0].description[0] = result.GoodreadsResponse.book[0].description[0].replace(re[i], "");

                    }
                    description = result.GoodreadsResponse.book[0].description[0];
                }

            })
            console.log(bookName);
            bookName.replace("%20", / /g);
            var output = bookName + " is authored by " + author_name + ". ";
            if(ratings_count!="" && ratings_count!=undefined)
            {
                output = output + "The ratings count is " + ratings_count + ". ";
            }
            if(average_rating!="" && average_rating!=undefined)
            {
                output = output + "And the average rating is " + average_rating + ". ";
            }
            if(year!="" && year!=undefined)
            {
                output = output + "It was published in the year " + year + ". ";
            }
            if(description!="" && description!=undefined)
            {
                 output = output + "Here goes a short description of the book. " + description;
            }
            
            Ddb.ddb_add_entry("Bookdetails_table", {
                "bookname": bookName,
                "details": output
            }, object, function(res, err) {
                if (res) {
                    console.log("Added to db");
                } else {
                    console.log("Unable to add");
                }
                sendDataToAlexa(object, ":tell", output);

            });
            return;
        });
    }
    getBookData.then(callbackBook).catch((err) => {
        object.emit(":tell", "Book not found");
    });
    return;
}

var goodreads_followAuthor = function(authorName, object) {

    // console.log(JSON.stringify(object.event.context.System.user.accessToken, null, 4))

    var authorID = "";
    var formattedAuthorName = authorName.replace(/ /g, "%20");

    var access_token = object.event.session.user.accessToken.split(",")[0]
    var access_token_secret = object.event.session.user.accessToken.split(",")[1]

    var getAuthorID = new Promise(function(resolve, reject) {
        var options = {
            "method": "GET",
            "url": HOST_NAME + "/api/author_url/+" + formattedAuthorName + "?" + KEY
        };
        http_request(options, function(error, response, body) {
            parseString(body.toString(), function(err, result) {
                if (result.GoodreadsResponse.author != undefined) {
                    authorID = result.GoodreadsResponse.author[0].$.id;
                }

            })
            if (authorID != "") {
                resolve(authorID);
            } else {
                reject(error);
            }

        });
    });

    callbackFollowAuthor = function() {
        oauth = {
            consumer_key: "rZPmFOvjHV6Mt12tn0lw",
            consumer_secret: "bilqTsvBgwWtPHMHwWiRoPei9LyRhKSN8tEzXQjGKU",
            token: access_token,
            token_secret: access_token_secret
        };
        url = 'https://www.goodreads.com/author_followings';
        qs = {
            id: authorID,
            format: 'xml'
        };
        console.log("Hello");
        http_request({
            method: 'POST',
            url: url,
            oauth: oauth,
            qs: qs
        }, function(error, r, body) {
            console.log("H inside callback");
            if (error) {
                console.log(error);
            } else {
                console.log(body);
                sendDataToAlexa(object, ":tell", "Following author " + authorName.replace("%20", / /g));
                return;
            }

        });
    }

    getAuthorID.then(callbackFollowAuthor).catch((err) => {
        object.emit(":tell", "Author not found. Please try again");

    });
    return;
}

var goodreads_booksOwned = function(object) {

    // var user_id = userDetails(context);
    var user_id = "";
    var userDetails = new Promise(function(resolve, reject) {
        var access_token = object.event.session.user.accessToken.split(",")[0];
        var access_token_secret = object.event.session.user.accessToken.split(",")[1];

        var parseString = require('../src/node_modules/xml2js').parseString;

        const myCredentials = {
            key: 'rZPmFOvjHV6Mt12tn0lw',
            secret: 'bilqTsvBgwWtPHMHwWiRoPei9LyRhKSN8tEzXQjGKU'
        };

        // AFAIK, goodreads doesn't support OAuth2.0
        var OAuth = require('oauth').OAuth;

        var OAUTH = new OAuth("http://www.goodreads.com/oauth/request_token", "http://www.goodreads.com/oauth/access_token", myCredentials.key, myCredentials.secret, '1.0', "http://localhost:3000", 'HMAC-SHA1');

        OAUTH.get("https://www.goodreads.com/api/auth_user", access_token, access_token_secret, function(error, response) {
            if (error) {
                console.log("Oops! Invalid access token.")
                reject(error);
            } else {
                console.log("Yay! \n" + response);
                parseString(response.toString(), function(err, result) {
                    var user_id = result.GoodreadsResponse.user[0].$.id
                    console.log(user_id);
                    resolve(user_id);
                })
            }
        })
    });
    var callbackOwnedBooks = function() {
        var access_token = object.event.session.user.accessToken.split(",")[0];
        var access_token_secret = object.event.session.user.accessToken.split(",")[1];

        var parseString = require('../src/node_modules/xml2js').parseString;

        const myCredentials = {
            key: 'rZPmFOvjHV6Mt12tn0lw',
            secret: 'bilqTsvBgwWtPHMHwWiRoPei9LyRhKSN8tEzXQjGKU'
        };

        // AFAIK, goodreads doesn't support OAuth2.0
        var OAuth = require('oauth').OAuth;

        var OAUTH = new OAuth("http://www.goodreads.com/oauth/request_token", "http://www.goodreads.com/oauth/access_token", myCredentials.key, myCredentials.secret, '1.0', "http://localhost:3000", 'HMAC-SHA1');

        OAUTH.get("https://www.goodreads.com/owned_books/user?format=xml&id=" + user_id, access_token, access_token_secret, function(error, response) {
            if (error) {
                console.log("Oops! Invalid access token.");
                return;
            } else {
                // console.log("Yay! \n" + response);
                parseString(response.toString(), function(err, result) {
                    if (result.GoodreadsResponse.owned_books[0] == "\n") {
                        console.log("No books owned :(");
                        outputSpeech = "No books found in your shelf.";
                    } else {
                        var count_of_books = result.GoodreadsResponse.owned_books[0].owned_book.length;
                        var owned_books = {};
                        for (book = 0; book < count_of_books; book++) {
                            console.log(result.GoodreadsResponse.owned_books[0].owned_book[book].book[0]);
                            console.log(result.GoodreadsResponse.owned_books[0].owned_book[book].book[0].authors[0].author);
                            owned_books[result.GoodreadsResponse.owned_books[0].owned_book[book].book[0].title_without_series[0]] = result.GoodreadsResponse.owned_books[0].owned_book[book].book[0].authors[0].author[0].name;
                        }
                        console.log(JSON.stringify("Owned books : " + owned_books));
                        var outputSpeech = "You own the following books. ";
                        for (book in owned_books) {
                            outputSpeech = outputSpeech + book + " by " + owned_books[book] + ". ";
                        }
                    }
                    sendDataToAlexa(object, ":tell", outputSpeech);
                    return;
                })
            }
        })
    };

    userDetails.then(callbackOwnedBooks).catch((err) => {
        object.emit(":tell", "Could not retrieve books. Please try again");

    });

}


var sendDataToAlexa = function(context, type, outputSpeech) {
        context.emit(type, outputSpeech);
    }

module.exports = {
    goodreads_getAuthorDetails: goodreads_getAuthorDetails,
    goodreads_getBookDetails: goodreads_getBookDetails,
    goodreads_followAuthor: goodreads_followAuthor,
    goodreads_booksOwned: goodreads_booksOwned,
    sendDataToAlexa: sendDataToAlexa
};
