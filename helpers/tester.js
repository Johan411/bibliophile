var ddb_crud = require("./dynamodb_crud");

var AWS = require("../src/node_modules/aws-sdk");

// use this only for testing & ensure that dynamodb is running locally on port 8000
AWS.config.update({
    region: "us-west-2",
    endpoint: "http://localhost:8000"
});

// Use this for creating tables

var params = {
    TableName: "Authordetails_table",
    KeySchema: [{
        AttributeName: "authorname",
        KeyType: "HASH"
    }],
    AttributeDefinitions: [{
        AttributeName: "authorname",
        AttributeType: "S"
    }],
    ProvisionedThroughput: {
        ReadCapacityUnits: 5,
        WriteCapacityUnits: 5
    }
};

function hello() {
    var docClient = new AWS.DynamoDB.DocumentClient()

    var table = "Authordetails_table";

    var params = {
        TableName: table,
        Key: {
            "authorname": "Robert Frost"
        }
    };

    docClient.get(params, function(err, data) {
        if (err) {
            console.error("Unable to read item. Error JSON:", JSON.stringify(err, null, 2));
        } else {
            console.log("GetItem succeeded:", JSON.stringify(data, null, 2));
        }
    });
}

// hello();

// ddb_crud.ddb_read_entry("test_table1", {authorid: "25"});

function foobar(context, data) {
    if (data == null)
        console.log("Not present in db");
    else {
        console.log(data);
    }
};

// ddb_crud.ddb_add_entry("Authordetails_table", {authorname: "Robert Frost", "details": "hello world"}, this, foobar);
ddb_crud.ddb_read_entry("Authordetails_table", {
    "authorname": "Robert"
}, this, foobar);