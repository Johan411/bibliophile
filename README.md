Bibliophile: Alexa-Goodreads companion
======================================

Bibliophile integrates Alexa with the social cataloging website, Goodreads. There was a whitespace between Alexa and Goodreads that could be filled in, which we have tried to narrow down. Currently, the skill allows the user to ask Alexa to do the following:
• Get the details of a book, like author, synopsis, rating
eg: "Alexa, ask bibliophile daily to get details of book And Then There Were None"
• Get the details of an author, like his/her hometown, popular book and a description of the same.
eg: "Alexa, ask bibliophile daily to get details of author Robert Frost"
• Log in as a Goodreads registered user using OAuth (this requires account linking)
• A logged in user gets to:
1. View books in his/her owned books list.
eg: "Alexa, ask bibliophile daily to get books owned by me"
2. Follow an author.
eg: "Alexa, ask bibliophile daily to follow author Robert Frost"

Credits for the icon: Icon made by Freepik from www.flaticon.com.
